<?php

use Carbon_Fields\Carbon_Fields;
use Carbon_Field_Extended_Select\Extended_Select_Field;

define( 'Carbon_Field_Extended_Select\\DIR', __DIR__ );

Carbon_Fields::extend( Extended_Select_Field::class, function( $container ) {

	return new Extended_Select_Field(
		$container['arguments']['type'],
		$container['arguments']['name'],
		$container['arguments']['label']
	);
} );
