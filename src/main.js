/**
 * External dependencies.
 */
import SelectField from "../vendor/htmlburger/carbon-fields/packages/core/fields/select";

class ExtendedSelectField extends SelectField {}

export default ExtendedSelectField;
