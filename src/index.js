/**
 * External dependencies.
 */
import { registerFieldType } from "@carbon-fields/core";

/**
 * Internal dependencies.
 */
import "./style.scss";
import ExtendedSelectField from "./main";

registerFieldType("extended_select", ExtendedSelectField);
